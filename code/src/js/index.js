menu = {
    id: null,
    content_id: null,
    showWebMenu: function () {
        //change selected tab
        $('.tab').removeClass('selected');
        $('#'+this.id).addClass('selected');

        //hide menu content
        $('.showContent').removeClass('showContent').addClass('hiddenContent');

        //show selected content
        $('#'+this.content_id).addClass('showContent')
    },
    showMobilePage: function () {
        //show mobile slider
        $('#wrapper').css('width','200vw');
        $('#mobileSlider').css('display','block');

        //fill the content
        this.getContent();

        $('#wrapper').animate({
            'right': '102vw'
        },1000)
    },
    returnWebPage: function () {
        $('#wrapper').animate({
            'right': '2vw'
        },1000)
        setTimeout("$('#wrapper').css('width','100%')",1200);
        setTimeout("$('#mobileSlider').css('display','none')",1200);
    },
    getContent: function () {
        data = $('#'+this.content_id).html()
        $('#mobileContent').html(data)
    }
}

//desktop show menu
$('#webMenu .tab').click(function () {
    //get tab id
    menu.id = $(this).get(0).id;

    //get content id
    menu.content_id = $(this).data('content_id');
    menu.showWebMenu();
});

$('#mobileMenu .tab').click(function () {
    //get content id
    menu.content_id = $(this).data('content_id');

    menu.showMobilePage()
})

//mobile back button
$('#back').click(function () {
    menu.returnWebPage();
})

    //reset to default on web view
$(window).on('resize', function(){
    if ($(this).width() >= 960) {
        $('#mobileSlider').css('display','none');
    }
    if ($(this).width() < 960) {
        $('#mobileSlider').css('display','block');
    }
});

