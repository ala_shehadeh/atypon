// Set the browser that you want to support
const AUTOPREFIXER_BROWSERS = [
    'ie >= 10',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10'
];

var gulp = require('gulp')
var autoprefixer = require('gulp-autoprefixer');
var htmlmin = require('gulp-htmlmin');
var csso = require('gulp-csso');
var uglify = require('gulp-uglify');
var del = require('del');
var runSequence = require('run-sequence');

// Clean output directory
gulp.task('clean', function () {
    return del(['dist']);
});

//copy fonts
gulp.task('fonts',function () {
    return gulp.src('./src/css/font/*')
        .pipe(gulp.dest('./dist/css/font'));
})

//copy images without compression
gulp.task('images',function () {
    return gulp.src('./src/images/*')
        .pipe(gulp.dest('./dist/images'));
})


//minify css
gulp.task('styles', function () {
    return gulp.src('./src/css/style.css')
    // Auto-prefix css styles for cross browser compatibility
        .pipe(autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
        // Minify the file
        .pipe(csso())
        // Output
        .pipe(gulp.dest('./dist/css'))
});

// Gulp task to minify JavaScript files
gulp.task('scripts', function() {
    return gulp.src('./src/js/*.js')
    // Minify the file
        .pipe(uglify())
        // Output
        .pipe(gulp.dest('./dist/js'))
});

// Gulp task to minify HTML files
gulp.task('pages', function() {
    return gulp.src(['./src/*.html'])
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true
        }))
        .pipe(gulp.dest('./dist'));
});

gulp.task('minify', function(callback) {
    runSequence(['clean','pages','styles','scripts','fonts','images']);
});
